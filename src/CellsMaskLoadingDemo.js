import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsMaskLoadingDemo-styles.js';
import { loading } from './loading.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-mask-loading-demo></cells-mask-loading-demo>
```

##styling-doc

@customElement cells-mask-loading-demo
*/
export class CellsMaskLoadingDemo extends LitElement {
  static get is() {
    return 'cells-mask-loading-demo';
  }

  // Declare properties
  static get properties() {
    return {
      show: { type: Boolean, attribute: 'show' }
    };
  }

  // Initialize properties
  constructor() {
    super();
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-mask-loading-demo-shared-styles')
    ];
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <div ?hidden="${!this.show}" class="container-loading">
        ${loading}
      </div>
    `;
  }
}

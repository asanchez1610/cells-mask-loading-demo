import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../cells-mask-loading-demo.js';

suite('CellsMaskLoadingDemo', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-mask-loading-demo></cells-mask-loading-demo>`);
    await el.updateComplete;
  });

  test('instantiating the element with default properties works', () => {
    const element = el.shadowRoot.querySelector('.container-loading');
    assert.isAccessible(element);
  });

});
